#!/bin/sh -ve
flutter config --enable-linux-desktop
# Try to fix the arm64 build
# See e.g. https://github.com/flutter/flutter/issues/116703#issuecomment-1403956612
if [[ $(uname -m) == "arm64" ]]; then
  curl -O https://storage.googleapis.com/flutter_infra_release/releases/stable/linux/flutter_linux_${FLUTTER_VERSION}-stable.tar.xz
  tar -xvf flutter_linux_${FLUTTER_VERSION}-stable.tar.xz flutter/bin/cache/artifacts/engine/linux-x64/shader_lib
  cp -R flutter/bin/cache/artifacts/engine/linux-x64/shader_lib ${FLUTTER_HOME}/bin/cache/artifacts/engine/linux-arm64
  rm -rf flutter flutter_linux_${FLUTTER_VERSION}-stable.tar.xz
fi
flutter clean
flutter pub get
flutter build linux --release -v
